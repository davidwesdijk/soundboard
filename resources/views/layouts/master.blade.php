@php
    $version = '1.0';
@endphp

<!DOCTYPE html>
<html class="full" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    @hasSection('title')
        <title>@yield('title') - Soundboard Favor Media</title>
    @else
        <title>Soundboard Favor Media</title>
    @endif

    <link rel="manifest" href="/manifest.json">
    <link rel="apple-touch-icon" href="/img/touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/touch-icon-ipad-retina.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">


    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/soundboard.css?v={{ $version }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    @stack('styles')
</head>

<body id="page-top" class="index">

<nav class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <span class="uber">UBER</span>
                <span class="soundboard">SOUNDBOARD</span>
                @isset($identifier)
                    <span class="label label-primary">Remote: {{ $identifier }}</span>
                @endisset
            </a>
        </div>
    </div>
</nav>

<div class="container">
    @if (session('success'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Sluiten</span>
                    </button>
                    {{ session('success') }}
                </div>
            </div>
        </div>
    @endif

    @yield('content')
</div>

@stack ('modal')

<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
@stack('scripts')
</body>
</html>
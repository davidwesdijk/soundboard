<?php

namespace App\Http\Controllers;

use App\Helpers\SoundHelper;
use App\Sound;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class SoundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sounds = Sound::orderBy('counter', 'desc')->get();

        return view('sound-index', compact('sounds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->hasFile('image') || !$request->hasFile('upload_file')) {
            return redirect()->back(400);
        }

        SoundHelper::createNewSound($request);

        return redirect()->to(route('sound.index'))->with('success', 'Successfully added!');
    }

    /**
     * @param Sound $sound
     *
     * @return mixed
     */
    public function play(Sound $sound)
    {
        return response($sound->getSoundFile())
            ->header('Content-Type', 'audio/mpeg');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SoundController@index')->name('sound.index');
Route::get('/remote/{identifier}', 'RemoteController@client')->name('remote.client');
Route::get('/remote/{identifier}/processor', 'RemoteController@processor')->name('remote.processor');
Route::post('/remote/{identifier}', 'RemoteController@pollQueue')->name('remote.poll');
Route::post('/remote/{identifier}/{sound}', 'RemoteController@addToQueue')->name('remote.addToQueue');
Route::get('/statistics', 'CounterController@index')->name('counter.index');
Route::get('/{sound}', 'SoundController@play')->name('sound.play');
Route::get('/{sound}/addCounter', 'CounterController@addCounter')->name('counter.add');
Route::post('/', 'SoundController@store')->name('sound.store');

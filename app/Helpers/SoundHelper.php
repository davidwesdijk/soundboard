<?php

namespace App\Helpers;

use App\Sound;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class SoundHelper
{
    /**
     * @param Request $request
     *
     * @return mixed
     */
    public static function createNewSound(Request $request)
    {
        $sound = Sound::create([
            'title' => $request->title
        ]);

        $request->upload_file->move(storage_path('app/public/sound/'), $sound->id . '.mp3');
        $sound->path = 'sound/' . $sound->id . '.mp3';

        Image::make($request->image->getRealPath())
            ->fit(500)
            ->save(storage_path('app/public/thumb/' . $sound->id . '.' . $request->image->getClientOriginalExtension()));

        $sound->image = 'thumb/' . $sound->id . '.' . $request->image->getClientOriginalExtension();
        $sound->save();

        return $sound;
    }
}
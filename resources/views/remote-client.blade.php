@extends('layouts.master', ['remoteId' => $identifier])

@section('content')
    <div class="row" id="soundBox">

        @foreach ($sounds as $sound)
            <div class="col-md-2 col-xs-4">
                <div class="sound" id="{{ $sound->id }}" style="background-image: url('{{ $sound->getImageUri() }}');">
                    <div class="title">{{ str_limit($sound->title, 15) }}</div>
                </div>
            </div>
        @endforeach
    </div>
@stop

@push ('scripts')
    <script>
        $(function () {
            $('.sound').on('click', function (el) {
                var url = '{{ route('remote.addToQueue', [$identifier, '']) }}/' + $(this).attr('id');
                $.post(url, {})
                    .done(function (response) {
                        console.log('added to queue!');
                    });
            });
        });
    </script>
@endpush
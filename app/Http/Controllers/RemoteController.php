<?php

namespace App\Http\Controllers;

use App\Queue;
use App\Sound;
use Illuminate\Http\Request;

class RemoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function client(string $identifier)
    {
        $sounds = Sound::orderBy('counter', 'desc')->get();

        return view('remote-client', compact('sounds', 'identifier'));
    }

    /**
     * @param  string  $identifier
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function processor(string $identifier)
    {
        return view('remote-processor', compact('identifier'));
    }

    /**
     * @param  string  $identifier
     * @param  Sound  $sound
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToQueue(string $identifier, Sound $sound)
    {
        Queue::query()
            ->where('sound_id', $sound->id)
            ->where('identifier', $identifier)
            ->where('played', 0)
            ->delete();

        Queue::create([
            'sound_id' => $sound->id,
            'identifier' => $identifier,
        ]);

        return response()->json([
            'status' => 200,
        ]);
    }

    /**
     * @param  string  $identifier
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function pollQueue(string $identifier)
    {
        $queue = Queue::query()
            ->where('identifier', $identifier)
            ->where('played', 0)
            ->oldest()->first();

        if ($queue) {
            $queue->played = true;
            $queue->save();

            if ($queue->sound) {
                $queue->sound->addCounter();
            }

            return response()->json([
                'status' => 200,
                'sound' => true,
                'sound_id' => $queue->sound_id,
            ]);
        }

        return response()->json([
            'status' => 200,
            'sound' => false,
        ]);
    }
}

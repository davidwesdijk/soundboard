var preloadSounds = [];
var sounds = {};
var isMobile = false;

$(document).ready(function () {
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		isMobile = true;
	}

	initSounds();
    setInterval("getCounter()", 2000);
});

function initSounds() {
    $('.sound').click(function() {
        playSound($(this).attr('id'));
    });
}

function getCounter() {
    $.ajax({
        url: '/statistics',
        type: 'GET',
        dataType:'json',
        success: function(data) {
            $.each(data, function(key, counter) {
                $('#' + counter.id).find('.badge').text(counter.counter);
            });
        }
    });
}

function playSound(id) {
	$.each($('audio'), function (key, item) {
        try {
            item.pause();
            item.currentTime = 0;
        } catch (exception) {
            console.log('could not stop');
		}
	});
	    
	$('#' + id + ' audio').get(0).play();
	showSoundProgress(id);
	addCounter(id);
};

function addCounter(id) {
	$.ajax({
		type: 'GET',
		dataType: 'json',
		url: '/' + id + '/addCounter',
	});
}

var currentAnimation = null;
var currentProgress = null;

function showSoundProgress(id) {
	var sound = $('#' + id + ' audio').get(0);
	var el = $('#' + id);
	var container = el.closest('.sound');
	
	//if (sound.duration && !el.attr("data-isprogress")) {
	var duration = parseFloat(sound.duration) * 1000;
	
	el.attr('data-isprogress', '1');
	el.attr('data-mscurrent', duration);
			
	if (currentAnimation !== null) {
		currentAnimation.stop();
		currentProgress.fadeOut({
			complete: function() {
				$(this).css({
					width: 0
				});
				$(this).show();
				$(this).closest('.sound').find('[data-isprogress]').removeAttr('data-isprogress');
			}
		});
	}
	
	currentProgress = container.find('.progression');
	
	currentAnimation = container.find('.progression').animate({
		width: 164
	}, duration, 'linear', function() { 
		$(this).fadeOut({
			complete: function() {
				$(this).css({
					width: 0
				});
				$(this).show();
				$(this).closest('.sound').find('[data-isprogress]').removeAttr('data-isprogress');
			}
		});
	});
};

function editSound(id) {
	var el = $('#' + id);
	var name = el.find('.title').text();
	var counter = el.find('.badge').text();
	var cat;
	
	if (el.attr('data-category').search("temp_") > -1) {
		cat = '';
	} else {
		cat = el.attr('data-category');
	}
	
	// Add values to edit form
	$('#edit').find('#id').val(id);
	$('#edit').find('#name').val(name);
	$('#edit').find('#subCategory').val(cat);
	$('#edit').find('.count').html(counter);
	
	$('#edit').modal('show')
}

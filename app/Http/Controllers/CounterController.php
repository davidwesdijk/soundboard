<?php

namespace App\Http\Controllers;

use App\Sound;
use Illuminate\Http\Request;

class CounterController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $statistics = Sound::select(['id', 'counter'])->get();

        return response()->json($statistics);
    }

    /**
     * @param Sound $sound
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCounter(Sound $sound)
    {
        $sound->addCounter();

        return response()->json(['status' => 200]);
    }
}

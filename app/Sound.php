<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Sound extends Model
{
    use Uuids;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    protected $fillable = [
        'title',
        'path',
        'image',
        'counter',
    ];

    /**
     * @return string|null
     */
    public function getImageUri()
    {
        if (Storage::disk('public')->exists($this->image)) {
            return Storage::disk('public')->url($this->image);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function getSoundFile()
    {
        if (Storage::disk('public')->exists($this->path)) {
            return Storage::disk('public')->get($this->path);
        }

        return null;
    }

    public function addCounter()
    {
        $this->update([
            'counter' => $this->counter + 1
        ]);
    }
}

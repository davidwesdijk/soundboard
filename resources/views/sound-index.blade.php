@extends('layouts.master')

@section('content')
    <div class="row" id="soundBox">
        @foreach ($sounds as $sound)
            <div class="col-md-2 col-xs-4">
                <div class="sound" id="{{ $sound->id }}" style="background-image: url('{{ $sound->getImageUri() }}');">
                    <audio src="{{ route('sound.play', $sound) }}" type="audio/mp3"></audio>
                    <div class="title">{{ str_limit($sound->title, 15) }}</div>
                    <div class="options">
                        <span class="badge">{{ $sound->counter }}</span>
                    </div>
                    <div class="progression"></div>
                </div>
            </div>
        @endforeach
    </div>

    <button type="button" data-toggle="modal" data-target="#add" class="btn btn-success btn-circle">
        <i class="fa fa-plus"></i>
    </button>

    <script src="/js/soundboard.js"></script>
@stop

@push('modal')
    <form action="{{ route('sound.store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add sound</h4>
                    </div>
                    <div class="modal-body">
                        <div id="error_message"></div>
                        <div class="form-group">
                            <label for="name">Title of your epic sound</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                        </div>
                        <div class="type_image radio_switch">
                            <div class="form-group">
                                <label for="upload_file">Background-image</label>
                                <input type="file" accept="image/gif, image/jpeg, image/png" name="image"
                                       id="image">
                                <p class="help-block">Max. file size: 10 MB (PNG, JPG or animated GIFS =D)</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="upload_file">Upload your sound</label>
                            <input type="file" accept="audio/mp3" name="upload_file" id="sound">
                            <p class="help-block">Max. file size: 10 MB (mp3)</p>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" required="required"> David is the boss*
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endpush

@push('styles')
    <style>
        .btn-circle {
            width: 50px;
            height: 50px;
            text-align: center;
            padding: 6px 0;
            font-size: 25px;
            line-height: 1.428571429;
            border-radius: 50%;
            bottom: 100px;
            right: 50px;
            position: fixed;
        }
    </style>
@endpush
@extends('layouts.master', ['remoteId' => $identifier])

@section('content')
    <div class="row" id="soundBox">
        <audio src="" id="audio"></audio>

        <div class="col-sm-12 text-center">
            <i class="fa fa-spinner fa-5x fa-spin" style="margin-top: 200px;"></i>
            <div style="margin-top: 20px;">
                <em class="text-muted">Polling...</em>
            </div>
        </div>
    </div>
@stop

@push ('scripts')
    <script>
        var url = '{{ route('remote.poll', $identifier) }}';
        var isPlaying = false;
        var xhr;

        function poll() {
            if (!isPlaying) {
                if (xhr != null) {
                    xhr.abort();
                }

                xhr = $.post(url, {})
                    .done(function (response) {
                        if (response.sound == true) {
                            playSound(response.sound_id);
                        }
                    });
            }
        }

        function playSound(id) {
            isPlaying = true;

            $('#audio').attr('src', '/' + id);

            var promise = document.querySelector('audio').play();

            if (promise !== undefined) {
                promise.then(_ => {
                    //Started!
                }).catch(error => {
                    console.log(error);
                })
            }
        }

        $(function () {
            poll();
            setInterval(poll, 2500);

            $('#audio').on('playing', function() {
                isPlaying = true;
            });
            $('#audio').on('ended', function() {
                isPlaying = false;
            });
        });
    </script>
@endpush
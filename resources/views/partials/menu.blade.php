<li class="sidebar-nav-item">
    <a class="js-scroll-trigger" href="#page-top">Home</a>
</li>
<li class="sidebar-nav-item">
    <a class="js-scroll-trigger" href="#about">About</a>
</li>
<li class="sidebar-nav-item">
    <a class="js-scroll-trigger" href="#services">Services</a>
</li>
<li class="sidebar-nav-item">
    <a class="js-scroll-trigger" href="#portfolio">Portfolio</a>
</li>
<li class="sidebar-nav-item">
    <a class="js-scroll-trigger" href="#contact">Contact</a>
</li>
<?php

use App\Sound;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SoundSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sound::truncate();
        Storage::disk('public')->makeDirectory('sound');
        Storage::disk('public')->makeDirectory('thumb');

        $this->createSound('Spelen met prive', '56f4fb5e3ab8b0e04ff81993.mp3', '56f4fb5e3ab8b0e04ff81993.png');
        $this->createSound('Female orgasm', '56f39b693ab8b08a743e0c80.mp3', '56f39b693ab8b08a743e0c80.png');
        $this->createSound('Badum tss!', '56f39ba13ab8b04b553e0c7e.mp3', '56f39ba13ab8b04b553e0c7e.jpg');
        $this->createSound('Bier!', '56f50f943ab8b0b27df8198f.mp3', '56f50f943ab8b0b27df8198f.jpg');
        $this->createSound('Niet vanzelf', '56f55a5a3ab8b0e434f81991.mp3', '56f55a5a3ab8b0e434f81991.jpg');
        $this->createSound('Handtekening!', '56f55d4d3ab8b0e434f81992.mp3', '56f55d4d3ab8b0e434f81992.jpg');
        $this->createSound('Fart', '56f3024b3ab8b0da0d7e3200.mp3', '56f3024b3ab8b0da0d7e3200.gif');
        $this->createSound('Whahahao', '56f409153ab8b09f2c3e0c7e.mp3', '56f409153ab8b09f2c3e0c7e.gif');
        $this->createSound('Inception', '570b5d993ab8b06c4e885e56.mp3', '570b5d993ab8b06c4e885e56.gif');
        $this->createSound('Beter he!', '570b66123ab8b0626a885e55.mp3', '570b66123ab8b0626a885e55.jpg');
        $this->createSound('Turbo', '570bb8973ab8b03f46885e55.mp3', '570bb8973ab8b03f46885e55.jpg');
        $this->createSound('Bye bye man', '570ca0d63ab8b0cd33369b91.mp3', '570ca0d63ab8b0cd33369b91.jpg');
        $this->createSound('Geintje natuurlijk', '570ca1f63ab8b08131369b94.mp3', '570ca1f63ab8b08131369b94.jpg');
        $this->createSound('Denk \'t niet', 'denktniet.mp3', 'denktniet.jpg');
        $this->createSound('Koekeroend', 'koekeroend.mp3', 'koekeroend.gif');
        $this->createSound('Of toch niet', 'tochniet.mp3', 'tochniet.gif');
        $this->createSound('Peter', 'f89d7d70-709f-11e8-ac21-1730126b35e1.mp3', 'f89d7d70-709f-11e8-ac21-1730126b35e1.jpg');
        $this->createSound('Kinderpindakaas', '038c0660-70a0-11e8-a7a4-8755a56a7ec5.mp3', '038c0660-70a0-11e8-a7a4-8755a56a7ec5.jpg');

        $this->createSound('570ca4aa3ab8b0292d369b94', '570ca4aa3ab8b0292d369b94.mp3', '570ca4aa3ab8b0292d369b94.jpg');
        $this->createSound('570ca4c63ab8b08131369b96', '570ca4c63ab8b08131369b96.mp3', '570ca4c63ab8b08131369b96.jpg');
        $this->createSound('570ca4ed3ab8b0362c369b93', '570ca4ed3ab8b0362c369b93.mp3', '570ca4ed3ab8b0362c369b93.jpg');
        $this->createSound('570ca44c3ab8b0292d369b93', '570ca44c3ab8b0292d369b93.mp3', '570ca44c3ab8b0292d369b93.jpg');
        $this->createSound('570ca54d3ab8b08131369b98', '570ca54d3ab8b08131369b98.mp3', '570ca54d3ab8b08131369b98.jpg');
        $this->createSound('570ca1453ab8b08131369b93', '570ca1453ab8b08131369b93.mp3', '570ca1453ab8b08131369b93.jpg');
        $this->createSound('570ca5103ab8b08131369b97', '570ca5103ab8b08131369b97.mp3', '570ca5103ab8b08131369b97.jpg');
        $this->createSound('570ca5663ab8b07030369b91', '570ca5663ab8b07030369b91.mp3', '570ca5663ab8b07030369b91.jpg');
        $this->createSound('570ca6113ab8b0292d369b95', '570ca6113ab8b0292d369b95.mp3', '570ca6113ab8b0292d369b95.gif');
        $this->createSound('570ca7273ab8b08131369b99', '570ca7273ab8b08131369b99.mp3', '570ca7273ab8b08131369b99.jpg');
        $this->createSound('570ca9633ab8b08131369b9a', '570ca9633ab8b08131369b9a.mp3', '570ca9633ab8b08131369b9a.jpg');
        $this->createSound('570d39333ab8b03450369b9c', '570d39333ab8b03450369b9c.mp3', '570d39333ab8b03450369b9c.jpg');
        $this->createSound('570e3cdf3ab8b06b38f20beb', '570e3cdf3ab8b06b38f20beb.mp3', '570e3cdf3ab8b06b38f20beb.jpg');
        $this->createSound('570e4c903ab8b0f147f20bec', '570e4c903ab8b0f147f20bec.mp3', '570e4c903ab8b0f147f20bec.png');
        $this->createSound('570e4f963ab8b0d136f20beb', '570e4f963ab8b0d136f20beb.mp3', '570e4f963ab8b0d136f20beb.jpg');
        $this->createSound('570e513c3ab8b0e640f20bec', '570e513c3ab8b0e640f20bec.mp3', '570e513c3ab8b0e640f20bec.gif');
        $this->createSound('570f99a23ab8b0ae6829e761', '570f99a23ab8b0ae6829e761.mp3', '570f99a23ab8b0ae6829e761.jpg');
        $this->createSound('570fa92b3ab8b0a54829e767', '570fa92b3ab8b0a54829e767.mp3', '570fa92b3ab8b0a54829e767.jpg');
        $this->createSound('571a2dac3ab8b0b53206aed2', '571a2dac3ab8b0b53206aed2.mp3', '571a2dac3ab8b0b53206aed2.jpg');
        $this->createSound('571f30f83ab8b00616a55e95', '571f30f83ab8b00616a55e95.mp3', '571f30f83ab8b00616a55e95.jpg');
        $this->createSound('573c4e943ab8b0f93bfc5ff7', '573c4e943ab8b0f93bfc5ff7.mp3', '573c4e943ab8b0f93bfc5ff7.png');
        $this->createSound('573c39bc3ab8b05345fc5fe5', '573c39bc3ab8b05345fc5fe5.mp3', '573c39bc3ab8b05345fc5fe5.jpg');
        $this->createSound('573c39eb3ab8b03328fc5fe8', '573c39eb3ab8b03328fc5fe8.mp3', '573c39eb3ab8b03328fc5fe8.jpg');
        $this->createSound('573c284d3ab8b0fb3bfc5ff2', '573c284d3ab8b0fb3bfc5ff2.mp3', '573c284d3ab8b0fb3bfc5ff2.jpg');
        $this->createSound('573da3273ab8b02d15790cc4', '573da3273ab8b02d15790cc4.mp3', '573da3273ab8b02d15790cc4.png');
        $this->createSound('573e333b3ab8b06a5f790cb8', '573e333b3ab8b06a5f790cb8.mp3', '573e333b3ab8b06a5f790cb8.png');
        $this->createSound('573ef2b83ab8b08b13d943bb', '573ef2b83ab8b08b13d943bb.mp3', '573ef2b83ab8b08b13d943bb.png');
        $this->createSound('573ef5bf3ab8b0f36cd943bb', '573ef5bf3ab8b0f36cd943bb.mp3', '573ef5bf3ab8b0f36cd943bb.jpg');
        $this->createSound('573ef23b3ab8b0f66cd943b7', '573ef23b3ab8b0f66cd943b7.mp3', '573ef23b3ab8b0f66cd943b7.png');
        $this->createSound('573f0e2f3ab8b04f3cd943a9', '573f0e2f3ab8b04f3cd943a9.mp3', '573f0e2f3ab8b04f3cd943a9.jpg');
        $this->createSound('577ba1803ab8b0ba50fadec6', '577ba1803ab8b0ba50fadec6.mp3', '577ba1803ab8b0ba50fadec6.gif');
        $this->createSound('5707ad323ab8b00b13a0569c', '5707ad323ab8b00b13a0569c.mp3', '5707ad323ab8b00b13a0569c.jpg');
        $this->createSound('5714b2ca3ab8b0574fbdfa21', '5714b2ca3ab8b0574fbdfa21.mp3', '5714b2ca3ab8b0574fbdfa21.jpg');
        $this->createSound('5736f3d63ab8b0425860590e', '5736f3d63ab8b0425860590e.mp3', '5736f3d63ab8b0425860590e.png');
        $this->createSound('5736f3fa3ab8b0d71160590d', '5736f3fa3ab8b0d71160590d.mp3', '5736f3fa3ab8b0d71160590d.png');
        $this->createSound('5736f56a3ab8b0fa5760590c', '5736f56a3ab8b0fa5760590c.mp3', '5736f56a3ab8b0fa5760590c.png');
        $this->createSound('5736f1383ab8b0fc57605913', '5736f1383ab8b0fc57605913.mp3', '5736f1383ab8b0fc57605913.png');
        $this->createSound('5736f2473ab8b0f857605910', '5736f2473ab8b0f857605910.mp3', '5736f2473ab8b0f857605910.png');
        $this->createSound('5740b5b53ab8b0cc4ca8ee79', '5740b5b53ab8b0cc4ca8ee79.mp3', '5740b5b53ab8b0cc4ca8ee79.png');
        $this->createSound('5741a2ad3ab8b02151d6baaa', '5741a2ad3ab8b02151d6baaa.mp3', '5741a2ad3ab8b02151d6baaa.png');
        $this->createSound('5741a3f73ab8b02051d6baad', '5741a3f73ab8b02051d6baad.mp3', '5741a3f73ab8b02051d6baad.png');
        $this->createSound('5746e1ca3ab8b0162ced9627', '5746e1ca3ab8b0162ced9627.mp3', '5746e1ca3ab8b0162ced9627.gif');
        $this->createSound('57381ad13ab8b03b312ca7e9', '57381ad13ab8b03b312ca7e9.mp3', '57381ad13ab8b03b312ca7e9.png');
        $this->createSound('57382c7c3ab8b03b312ca7ec', '57382c7c3ab8b03b312ca7ec.mp3', '57382c7c3ab8b03b312ca7ec.png');
        $this->createSound('57482c533ab8b057751a8026', '57482c533ab8b057751a8026.mp3', '57482c533ab8b057751a8026.png');
        $this->createSound('57580d0e3ab8b024226d16cf', '57580d0e3ab8b024226d16cf.mp3', '57580d0e3ab8b024226d16cf.png');
        $this->createSound('57580d4e3ab8b0f0266d16c9', '57580d4e3ab8b0f0266d16c9.mp3', '57580d4e3ab8b0f0266d16c9.jpg');
        $this->createSound('573733d53ab8b0fb57605917', '573733d53ab8b0fb57605917.mp3', '573733d53ab8b0fb57605917.png');
        $this->createSound('573733fe3ab8b0f95760591c', '573733fe3ab8b0f95760591c.mp3', '573733fe3ab8b0f95760591c.png');
        $this->createSound('573734ad3ab8b0f95760591d', '573734ad3ab8b0f95760591d.mp3', '573734ad3ab8b0f95760591d.png');
        $this->createSound('574080f33ab8b0be46a8ee76', '574080f33ab8b0be46a8ee76.mp3', '574080f33ab8b0be46a8ee76.jpg');
        $this->createSound('5737342f3ab8b0f857605914', '5737342f3ab8b0f857605914.mp3', '5737342f3ab8b0f857605914.png');
        $this->createSound('573733143ab8b0247f605905', '573733143ab8b0247f605905.mp3', '573733143ab8b0247f605905.png');
        $this->createSound('573734623ab8b04258605914', '573734623ab8b04258605914.mp3', '573734623ab8b04258605914.png');
        $this->createSound('573734883ab8b04258605915', '573734883ab8b04258605915.mp3', '573734883ab8b04258605915.png');
        $this->createSound('574558103ab8b0a829d90bca', '574558103ab8b0a829d90bca.mp3', '574558103ab8b0a829d90bca.jpg');
    }

    /**
     * @param $title
     * @param $soundFile
     * @param $imageFile
     *
     * @return mixed
     */
    private function createSound($title, $soundFile, $imageFile)
    {
        $sound = Sound::updateOrCreate([
            'title' => $title
        ]);

        $sound->path = sprintf('sound/%s.mp3', $sound->id);
        $sound->image = sprintf('thumb/%s.png', $sound->id);

        $soundSeederFile = File::get(base_path(sprintf('database%2$sseeds%2$sfiles%2$s%1$s', $soundFile, DIRECTORY_SEPARATOR)));
        Storage::disk('public')->put($sound->path, $soundSeederFile);

        $imageSeederFile = Image::make(base_path(sprintf('database%2$sseeds%2$sfiles%2$s%1$s', $imageFile, DIRECTORY_SEPARATOR)))
            ->fit(500)
            ->save(storage_path('app/public/' . $sound->image));

        return $sound->save();
    }
}
